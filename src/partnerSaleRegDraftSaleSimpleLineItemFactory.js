import PartnerSaleRegDraftSaleSimpleLineItem from './partnerSaleRegDraftSaleSimpleLineItem';

export default class PartnerSaleRegDraftSaleSimpleLineItemFactory {

    static construct(data):PartnerSaleRegDraftSaleSimpleLineItem[] {

        let componentObj = [];

        Array.from(data,dataItem => {
            var component = {};
            if(dataItem.terms){
                component.terms = dataItem.terms;
                component.selectedPrice = dataItem.selectedPrice;
            }
            if(dataItem.price){
                component.price = dataItem.price;
            }
            if(dataItem.selectedTerms){
                component.selectedTerms = dataItem.selectedTerms;
            }
            if(dataItem.materialNumber){
                component.materialNumber = dataItem.materialNumber;
            }
            if(dataItem.productName){
                component.productName = dataItem.productName;
            }

            component.id = dataItem.id;
            component.assetId = dataItem.assetId;
            component.serialNumber = dataItem.serialNumber;
            component.productLineName = dataItem.productLineName;
            componentObj.push(component);
        });

        return componentObj;
    }
}
