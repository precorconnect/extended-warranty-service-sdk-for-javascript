import ExtendedWarrantySimpleLineItem from './extendedWarrantySimpleLineItem';
import ExtendedWarrantyCompositeLineItem from './extendedWarrantyCompositeLineItem';

/**
 * @class {AddExtendedWarrantyDraftReq}
 */
export default class AddExtendedWarrantyDraftReq{

    _partnerSaleRegistrationId:number;

    _facilityName:string;

    _partnerAccountId:string;

    _isSubmitted:boolean;

    _discountCode:string;

    _simpleLineItems:ExtendedWarrantySimpleLineItem[] ;

    _compositeLineItems:ExtendedWarrantyCompositeLineItem[];

    constructor(
        partnerSaleRegistrationId:number,
        facilityName:string,
        partnerAccountId:string,
        isSubmitted:boolean,
        discountCode:string,
        simpleLineItems:ExtendedWarrantySimpleLineItem[],
        compositeLineItems:ExtendedWarrantyCompositeLineItem[]
    ){

        if(!partnerSaleRegistrationId){
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

        if(!facilityName){
            throw new TypeError('facilityName required');
        }
        this._facilityName = facilityName;

        if(!partnerAccountId){
            throw new TypeError('partnerAccountId required');
        }
        this._partnerAccountId = partnerAccountId;

        this._isSubmitted = isSubmitted;

        this._discountCode = discountCode;

        this._simpleLineItems = simpleLineItems;

        this._compositeLineItems = compositeLineItems;

    }

    /**
     * getter methods
     */
    get partnerSaleRegistrationId():number{
        return this._partnerSaleRegistrationId;
    }

    get facilityName():string{
        return this._facilityName;
    }

    get partnerAccountId():string{
        return this._partnerAccountId;
    }

    get isSubmitted():boolean{
        return this._isSubmitted;
    }

    get discountCode():string{
        return this._discountCode;
    }

    get simpleLineItems():ExtendedWarrantySimpleLineItem[]{
        return this._simpleLineItems;
    }

    get compositeLineItems():ExtendedWarrantyCompositeLineItem[]{
        return this._compositeLineItems;
    }

    toJSON() {
        return {
            partnerSaleRegistrationId: this._partnerSaleRegistrationId,
            facilityName: this._facilityName,
            partnerAccountId: this._partnerAccountId,
            isSubmitted: this._isSubmitted,
            discountCode: this._discountCode,
            simpleLineItems: this._simpleLineItems,
            compositeLineItems: this._compositeLineItems
        }
    }

}

