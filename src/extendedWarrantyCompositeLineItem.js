import PartnerSaleLineItemComponent from './partnerSaleLineItemComponent';
/**
 * @class {ExtendedWarrantyCompositeLineItem}
 */
export default class ExtendedWarrantyCompositeLineItem{

    _id:number;

    _components:PartnerSaleLineItemComponent[];

    _terms:string;

    _price:number;

    _selectedTerms:string;

    _selectedPrice:number;

    _materialNumber:string;


    /**
     * @param {string} id
     * @param components
     * @param {string} terms
     * @param {number} price
     * @param {string} selectedTerms
     * @param {number} selectedPrice
     * @param {string} materialNumber
     */
    constructor(id:number,
                components:PartnerSaleLineItemComponent[],
                terms:string,
                price:number,
                selectedTerms:string,
                selectedPrice:number,
                materialNumber:string
    ){

        this._id = id;

        if(!components){
            throw new TypeError('components required');
        }
        this._components = components;

        this._terms = terms;

        this._price = price;

        this._selectedTerms = selectedTerms;

        this._selectedPrice = selectedPrice;

        if(!materialNumber){
            throw new TypeError('materialNumber required');
        }
        this._materialNumber = materialNumber;

    }

    /**
     *
     * @returns {number}
     */
    get id():number{
        return this._id;
    }

    get components():PartnerSaleLineItemComponent[]{
        return this._components;
    }

    get terms():string{
        return this._terms;
    }

    get price():number{
        return this._price;
    }

    get selectedTerms():string{
        return this._selectedTerms;
    }

    get selectedPrice():number{
        return this._selectedPrice;
    }

    get materialNumber():string {
        return this._materialNumber;
    }

    toJSON(){
        return {
            id:this._id,
            components:this._components,
            terms:this._terms,
            price:this._price,
            selectedTerms:this._selectedTerms,
            selectedPrice:this._selectedPrice,
            materialNumber:this._materialNumber
        }
    }
}
