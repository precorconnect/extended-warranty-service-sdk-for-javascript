import PartnerSaleLineItemComponent from './partnerSaleLineItemComponent';
/**
 * @class {PartnerSaleRegDraftSaleCompositeLineItem}
 */
export default class PartnerSaleRegDraftSaleCompositeLineItem{

    _id:number;

    _components:PartnerSaleLineItemComponent[];

     /**
     * @param id
     * @param components
     */
    constructor(id:number,
                components:PartnerSaleLineItemComponent[]
    ){

        this._id = id;

        if(!components){
            throw new TypeError('components required');
        }
        this._components = components;

    }

    /**
     *
     * @returns {number}
     */
    get id():number{
        return this._id;
    }

    get components():PartnerSaleLineItemComponent[]{
        return this._components;
    }

    toJSON(){
        return {
            id:this._id,
            components:this._components
        }
    }
}
