import PartnerSaleRegDraftSaleSimpleLineItem from './partnerSaleRegDraftSaleSimpleLineItem';
import PartnerSaleRegDraftSaleCompositeLineItem from './partnerSaleRegDraftSaleCompositeLineItem';

/**
 * @class {PartnerCommercialSaleRegSynopsisView}
 */

export default class PartnerCommercialSaleRegSynopsisView{

    _id:number;

    _facilityName:string;

    _partnerAccountId:string;

    _installDate:string;

    _draftSubmittedDate:string;

    _partnerRepUserId:string;

    _simpleLineItems:PartnerSaleRegDraftSaleSimpleLineItem[];

    _compositeLineItems:PartnerSaleRegDraftSaleCompositeLineItem[];

    /**
     *
     * @param id
     * @param facilityName
     * @param partnerAccountId
     * @param installDate
     * @param draftSubmittedDate
     * @param partnerRepUserId
     * @param simpleLineItems
     * @param compositeLineItems
     */
    constructor(id:number,
                facilityName:string,
                partnerAccountId:string,
                installDate:string,
                draftSubmittedDate:string,
                partnerRepUserId:string,
                simpleLineItems:PartnerSaleRegDraftSaleSimpleLineItem[],
                compositeLineItems:PartnerSaleRegDraftSaleCompositeLineItem[]
    ){

        if(!id){
            throw new TypeError('id required');
        }
        this._id = id;

        if(!facilityName){
            throw new TypeError('facilityName required');
        }
        this._facilityName = facilityName;

        if(!partnerAccountId){
            throw new TypeError('partnerAccountId required');
        }
        this._partnerAccountId = partnerAccountId;

        if(!installDate){
            throw new TypeError('installDate required');
        }
        this._installDate = installDate;

        if(!draftSubmittedDate){
            throw new TypeError('draftSubmittedDate required');
        }
        this._draftSubmittedDate = draftSubmittedDate;

        this._partnerRepUserId = partnerRepUserId;

        this._simpleLineItems = simpleLineItems;

        this._compositeLineItems = compositeLineItems;

    }

    /**
     * getter methods
     */
    get id():number{
        return this._id;
    }

    get facilityName():string{
        return this._facilityName;
    }

    get partnerAccountId():string{
        return this._partnerAccountId;
    }

    get installDate():string{
        return this._installDate;
    }

    get draftSubmittedDate():string{
        return this._draftSubmittedDate;
    }

    get partnerRepUserId():string{
        return this._partnerRepUserId;
    }

    get simpleLineItems():PartnerSaleRegDraftSaleSimpleLineItem[]{
        return this._simpleLineItems;
    }

    get compositeLineItems():PartnerSaleRegDraftSaleCompositeLineItem[]{
        return this._compositeLineItems;
    }

    toJSON() {
        return {
            id: this._id,
            facilityName: this._facilityName,
            partnerAccountId: this._partnerAccountId,
            installDate: this._installDate,
            draftSubmittedDate: this._draftSubmittedDate,
            partnerRepUserId: this._partnerRepUserId,
            simpleLineItems: this._simpleLineItems,
            compositeLineItems: this._compositeLineItems
        }
    }

}

