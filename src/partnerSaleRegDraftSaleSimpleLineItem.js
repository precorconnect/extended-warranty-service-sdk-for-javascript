/**
 * @class {PartnerSaleRegDraftSaleSimpleLineItem}
 */
export default class PartnerSaleRegDraftSaleSimpleLineItem{

    _id:number;

    _assetId:string;

    _serialNumber:string;

    _productLineName:string;
    
    _productName:string;

    /**
     *
     * @param {number} id
     * @param {string} assetId
     * @param {string} serialNumber
     * @param {string} productLineName
     * @param {string} productName
     */
    constructor(id:number,
                assetId:string,
                serialNumber:string,
                productLineName:string,
                productName:string
    ){
        if(!id){
            throw new TypeError('id required');
        }
        this._id = id;

        if(!assetId){
            throw new TypeError('assetId required');
        }
        this._assetId = assetId;

        if(!serialNumber){
            throw new TypeError('serialNumber required');
        }
        this._serialNumber = serialNumber;

        if(!productLineName){
            throw new TypeError('productLineName required');
        }
        this._productLineName = productLineName;
        
        this._productName = productName;

    }

    /**
     * getter methods
     */
    get id():number{
        return this._id;
    }

    get assetId():string{
        return this._assetId;
    }

    get serialNumber():string{
        return this._serialNumber;
    }

    get productLineName():string{
        return this._productLineName;
    }

    get productName():string{
        return this._productName;
    }

    toJSON(){
        return {
            id:this._id,
            assetId:this._assetId,
            serialNumber:this._serialNumber,
            productLineName:this._productLineName,
            productName:this._productName
        }
    }
}
