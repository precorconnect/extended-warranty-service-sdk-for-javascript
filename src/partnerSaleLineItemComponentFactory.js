import PartnerSaleLineItemComponent from './partnerSaleLineItemComponent';

export default class PartnerSaleLineItemComponentFactory {

    static construct(data):PartnerSaleLineItemComponent[] {

        let componentObj = [];
        Array.from(data,dataItem => {
            var component = {};
            component.id = dataItem.id;
            component.assetId = dataItem.assetId;
            component.serialNumber = dataItem.serialNumber;
            component.productLineName = dataItem.productLineName;
            component.productName = dataItem.productName;
            componentObj.push(component);
        });

        return componentObj;
    }
}
