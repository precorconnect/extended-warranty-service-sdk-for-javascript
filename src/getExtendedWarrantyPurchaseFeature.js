import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ExtendedWarrantyDraftRes from './extendedWarrantyDraftRes';
import ExtendedWarrantyServiceSdkConfig from './extendedWarrantyServiceSdkConfig';
import GetExtendedWarrantyPurchaseFactory from './getExtendedWarrantyPurchaseFactory';

/**
 * @class {GetExtendedWarrantyPurchaseFeature}
 */
@inject(ExtendedWarrantyServiceSdkConfig, HttpClient)
class GetExtendedWarrantyPurchaseFeature {

    _config:ExtendedWarrantyServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ExtendedWarrantyServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    execute(partnerSaleRegistrationId:number,
            accessToken:string):Promise<ExtendedWarrantyDraftRes> {

        return this._httpClient
            .createRequest(`extended-warranty/getExtendedWarranty/partnerSaleRegistrationId/${partnerSaleRegistrationId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response =>
                GetExtendedWarrantyPurchaseFactory.construct(response.content)
            );
    }
}

export default GetExtendedWarrantyPurchaseFeature;

