import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ExtendedWarrantyServiceSdkConfig from './extendedWarrantyServiceSdkConfig';

/**
 * @class {CheckDiscountAvailOnExtendedWarrantyFeature}
 */
@inject(ExtendedWarrantyServiceSdkConfig, HttpClient)
class CheckDiscountAvailOnExtendedWarrantyFeature {

    _config:ExtendedWarrantyServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ExtendedWarrantyServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    execute(discountCode:string,
            accessToken:string):Promise<boolean> {

        return this._httpClient
            .createRequest(`extended-warranty/discountCode/${discountCode}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => response.content);

    }
}

export default CheckDiscountAvailOnExtendedWarrantyFeature;
