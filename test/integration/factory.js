import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';
import ExtendedWarrantySimpleLineItem from '../../src/extendedWarrantySimpleLineItem';
import ExtendedWarrantyCompositeLineItem from '../../src/extendedWarrantyCompositeLineItem';
import ExtendedWarrantyLineItemComponent from '../../src/extendedWarrantyLineItemComponent';

export default {
    constructValidPartnerRepOAuth2AccessToken,
    constructValidAppAccessToken,
    constructSimpleLineItems,
    constructCompositeLineItems
}

function constructValidPartnerRepOAuth2AccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "sub": dummy.url,
        "account_id": dummy.partnerAccountId,
        "sap_vendor_number": dummy.sap_vendor_number
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructValidAppAccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": "app",
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructSimpleLineItems():ExtendedWarrantySimpleLineItem[]{

    return [{
        assetId:dummy.assetId,
        serialNumber:dummy.serialNumber,
        productLineName:dummy.productLineName,
        terms:dummy.terms,
        price:dummy.price,
        selectedTerms:dummy.selectedTerms,
        selectedPrice:dummy.selectedPrice,
        materialNumber:dummy.materialNumber,
        productName:dummy.productName
        }]
    ;
}

function constructSaleLineItemComponents():ExtendedWarrantyLineItemComponent[]{
    return [{
        assetId:dummy.assetId,
        serialNumber:dummy.serialNumber,
        productLineName:dummy.productLineName,
        productName:dummy.productName
    }];
}

function constructCompositeLineItems():ExtendedWarrantyCompositeLineItem[]{
    return [{
        components:constructSaleLineItemComponents(),
        terms:dummy.terms,
        price:dummy.price,
        selectedTerms:dummy.selectedTerms,
        selectedPrice:dummy.selectedPrice,
        materialNumber:dummy.materialNumber
    }];
}

